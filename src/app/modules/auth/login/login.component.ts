import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { first, switchMap } from 'rxjs';
import { AlertService } from 'src/app/shared/auth/alert.service';
import { AuthService } from 'src/app/shared/auth/auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  hide = true;
  loginForm!: FormGroup;
  loading = false;
  submitted = false;
  returnUrl!: string;

  constructor(
    private formBuilder: FormBuilder,
    private auth: AuthService,
    private router: Router,
    private routerActivated: ActivatedRoute,
    private alertService: AlertService
  ) {
    if (this.auth.currentUserValue) {
      this.router.navigate(['/admin']);
    }
  }

  ngOnInit() {

    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
    this.returnUrl = this.routerActivated.snapshot.queryParams['returnUrl'] || '/admin';
  }

  get f(): any {
    return this.loginForm.controls;
  }

  onLogin() {
    this.submitted = true;
    this.alertService.clear();
    if (this.loginForm.invalid) {
      return;
    }

    // this.loading= true;
    this.auth.login(this.f.username.value, this.f.password.value)
      .pipe(switchMap(() => {
        return this.auth.getUser(this.f.username.value)
      }))
      .subscribe(

        data => {
          window.location.reload();
          this.auth.setCurrentUser(data.data)
          this.router.navigate(['/']);
          alert('Đăng nhập thành công')
        },
        error => {
          this.alertService.error(error);
          this.loading = false
          alert('Đăng nhập thất bại')
        }

      )

  }




}
