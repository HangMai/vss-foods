import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UserService } from 'src/app/shared/services/user.service';
import { UserNewComponent } from '../user-new/user-new.component';
import '../../../../assets/form.css';
import { AuthService } from 'src/app/shared/auth/auth.service';
import { first } from 'rxjs';
import { Post } from 'src/app/shared/post-Interface/post';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['../../../../assets/form.css']
})
export class UserListComponent implements OnInit {
  displayedColumns: string[] = ['username', 'fullName', 'email',
    'phoneNumber', 'address', 'roleName', 'status', 'createdAt', 'action'];
  dataSource!: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  users: Post[] = [];
  currentUser!: Post;


  constructor(
    private dialog: MatDialog,
    private userSevice: UserService,
    private auth: AuthService) {
    this.currentUser = this.auth.currentUserValue;
  }

  ngOnInit(): void {
    this.getAll()
  }
  getAll() {
    this.userSevice.getUser({})
      .subscribe({
        next: (res) => {
          this.dataSource = res.data;
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort
        },
        error: (err) => {
          console.log('Lỗi', err);

        }
      })
  }

  openDialog() {
    this.dialog.open(UserNewComponent, {
      width: '30%'
    }).afterClosed().subscribe(val => {
      if (val === "Save") {
        this.getAll();
      }
    })
  }
  onEdit(row: any) {
    this.dialog.open(UserNewComponent, {
      width: '30%',
      data: row
    }).afterClosed().subscribe(val => {
      if (val === "update") {
        this.getAll();
      }
    })


  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
}
