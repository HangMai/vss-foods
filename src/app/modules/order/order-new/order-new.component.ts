import { Component, Inject, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderService } from 'src/app/shared/services/order.service';

@Component({
  selector: 'app-order-new',
  templateUrl: './order-new.component.html',
  styleUrls: ['./order-new.component.css']
})
export class OrderNewComponent implements OnInit {
  actionBtn: string = "Save"
  orderForm: FormGroup = new FormGroup({})
  checked = false;
  orderDetailList!: FormArray;
  constructor(
    private order: OrderService,
    private formBuilder: FormBuilder,
    private router: Router,
    @Inject(MAT_DIALOG_DATA) public editData: any,
    private dialogRef: MatDialogRef<OrderNewComponent>) { }



  ngOnInit() {
    this.orderForm = this.formBuilder.group({
      address: ['', Validators.required],
      customerName: ['', Validators.required],
      description: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      orderDetailList: this.formBuilder.array([]

      )
    });

    if (this.editData) {
      this.actionBtn = "Update";

      this.orderForm.patchValue(this.editData);

    }
  }
  createItem(): FormGroup {
    return this.formBuilder.group({
      comboId: '',
      foodId: '',
      price: '',
      promotionId: '',
      quantity: ''
    })
  };
  addItem(): void {
    this.orderDetailList = this.orderForm.get('orderDetailList') as FormArray;
    this.orderDetailList.push(this.createItem());
  }
  get formArr() {
    return this.orderForm.get("orderDetailList") as FormArray;
  }
  addOrder() {
    if (this.orderForm.invalid) return;
    if (this.editData) {
      this.order.addOrder(this.orderForm.value).subscribe(x => {
      })
    }

  }
}
