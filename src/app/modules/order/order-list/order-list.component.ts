import { Component, OnInit, ViewChild } from '@angular/core';
import { OrderService } from 'src/app/shared/services/order.service';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { OrderNewComponent } from '../order-new/order-new.component';
import '../../../../assets/form.css';

@Component({
  selector: 'app-order-list',
  templateUrl: './order-list.component.html',
  styleUrls: ['../../../../assets/form.css']
})
export class OrderListComponent implements OnInit {
  displayedColumns: string[] = ['userId', 'customerName', 'address', 'phoneNumber', 'orderCode', 'orderDate', 'description', 'price', 'status', 'completeAt'];
  dataSource!: MatTableDataSource<any>;
  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  constructor(private dialog: MatDialog, private order: OrderService) {

  }
  ngOnInit(): void {
    this.getAll();
  }
  getAll() {
    this.order.getAllOrder({})
      .subscribe({
        next: (res) => {
          this.dataSource = res.data.orders;
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort
        },
        error: (err) => {
          console.log('Lỗi', err);

        }
      })
  }
  openDialog() {
    this.dialog.open(OrderNewComponent, {
      width: '30%'
    }).afterClosed().subscribe(val => {
      if (val === "Save") {
        this.getAll();
      }
    })
  }




  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


}
