import { inject, Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Observable } from "rxjs";
import { AuthService } from "./auth.service";

@Injectable({
    providedIn: 'root',
  })
export class JwtInterceptor implements HttpInterceptor{

    constructor(private auth: AuthService){}
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // throw new Error("Method not implemented.");

        let curentUser= this.auth.currentUserValue;
        if(curentUser && localStorage.getItem('token')){
            
            req = req.clone({
                setHeaders:{
                    Authorization: `Bearer ${localStorage.getItem('token')}`
                }
            })
        }
        return next.handle(req)
    }
    
}