export interface Post {
    username: string;
    fullName: string;
    email: string;
    phoneNumber: string;
    address: string;
    roleName: string;
    status: string;
    token: string;
}
