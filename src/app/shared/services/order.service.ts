import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class OrderService {
  deleteOrder(id: number) {
    throw new Error('Method not implemented.');
  }
  URL = environment.api;

  private modals: any[] = [];

  constructor(private http: HttpClient) { }


  getAllOrder(params: any): Observable<any> {
    return this.http.get<any>(this.URL + '/order/all?page=0&size=10');
  }



  addOrder(body: any) {
    return this.http.post<any>(this.URL + '/order', body);
  }



}
