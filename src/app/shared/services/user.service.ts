import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  URL = environment.api

  constructor(private http: HttpClient) { }

  getUser(username: any) {
    return this.http.get<any>(this.URL + '/user?username=' + username)
  }
  addPromotion(body: any) {
    return this.http.post<any>(this.URL + '/promotion', body);
  }

  updatePromotion(id: string | number, body: any) {
    return this.http.put<any>(`${this.URL}/promotion?id=${id}`, body);
  }

  deletePromotion(id: any) {
    return this.http.delete<any>(this.URL + '/promotion?id=' + id);
  }



}
